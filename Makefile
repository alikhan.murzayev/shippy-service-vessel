build:
	protoc -I. --go_out=plugins=micro:. proto/vessel/vessel.proto
	docker build -t shippy-service-vessel .

run:
	docker run -it --name shippy-service-vessel -p 50052:50051 -e MICRO_SERVER_ADDRESS=:50051 shippy-service-vessel

stop:
	docker stop shippy-service-vessel
	docker rm shippy-service-vessel

