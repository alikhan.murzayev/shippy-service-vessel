package main

import (
	"context"
	"errors"
	"github.com/micro/go-micro"
	pb "gitlab.com/alikhan.murzayev/shippy-service-vessel/proto/vessel"
	"log"
)

// ------------------- Repository ------------------- //

type Repository interface {
	FindAvailable(*pb.Specification) (*pb.Vessel, error)
	AddVessel(vessel *pb.Vessel) error
}
type repository struct {
	vessels []*pb.Vessel
}

func (repo *repository) FindAvailable(spec *pb.Specification) (*pb.Vessel, error) {

	for _, ves := range repo.vessels {
		if spec.Capacity <= ves.Capacity && spec.MaxWeight <= ves.MaxWeight {
			return ves, nil
		}
	}

	return nil, errors.New("no vessels found by that spec")
}

func (repo *repository) AddVessel(vessel *pb.Vessel) error {

	repo.vessels = append(repo.vessels, vessel)

	return nil
}

// --------------------- Service --------------------- //

type Service interface {
	FindAvailable(ctx context.Context, req *pb.Specification, resp *pb.Response) error
}

type service struct {
	repo Repository
}

func (s *service) FindAvailable(ctx context.Context, req *pb.Specification, resp *pb.Response) error {

	ves, err := s.repo.FindAvailable(req)
	if err != nil {
		return err
	}

	resp.Vessel = ves

	return nil
}

func NewService(repo Repository) Service {
	return &service{repo: repo}
}

func NewRepository() Repository {
	return &repository{}
}

func main() {

	vessels := []*pb.Vessel{
		&pb.Vessel{
			Id:        "vessel001",
			Capacity:  500,
			MaxWeight: 20000,
			Name:      "Boaty McBoatface",
			Available: false,
		},
	}

	repository := NewRepository()
	if err := repository.AddVessel(vessels[0]); err != nil {
		log.Fatalf("error while adding vessel: %v", err)
	}
	service := NewService(repository)

	srv := micro.NewService(micro.Name("shippy.consignment.vessel"))
	srv.Init()

	pb.RegisterVesselServiceHandler(srv.Server(), service)

	if err := srv.Run(); err != nil {
		log.Fatalf("error: %v", err)
	}

}
